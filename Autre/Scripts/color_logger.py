import os

ATTRIBUTES = dict(
        list(zip([
            'bold',
            'dark',
            '',
            'underline',
            'blink',
            '',
            'reverse',
            'concealed'
            ],
            list(range(1, 9))
            ))
        )
del ATTRIBUTES['']

HIGHLIGHTS = dict(
        list(zip([
            'on_grey',
            'on_red',
            'on_green',
            'on_yellow',
            'on_blue',
            'on_magenta',
            'on_cyan',
            'on_white'
            ],
            list(range(40, 48))
            ))
        )

COLORS = {
    'grey': '30;1',
    'red': '31',
    'green': '32',
    'yellow': '33;1',
    'blue': '34',
    'magenta': '35',
    'cyan': '36',
    'white': '31',
    'black': '30'
}

RESET = '\033[0m'

def color_text(text, color=None, on_color=None, attrs=None):
    if os.getenv('ANSI_COLORS_DISABLED') is None:
        fmt_str = '\033[%sm%s'
        if color is not None:
            text = fmt_str % (COLORS[color], text)

        if on_color is not None:
            text = fmt_str % (HIGHLIGHTS[on_color], text)

        if attrs is not None:
            for attr in attrs:
                text = fmt_str % (ATTRIBUTES[attr], text)

        text += RESET
    return text

def color_multiple_text(list_text_with_options):
    final_string = ''
    for part in list_text_with_options:
        if('color' not in part):
            part['color'] = None
        if('on_color' not in part):
            part['on_color'] = None
        if('attrs' not in part):
            part['attrs'] = None
        if('text' in part):
            final_string += color_text(part['text'], part['color'], part['on_color'], part['attrs'])
    return final_string

def format_text_part(text, color=None, on_color=None, attrs=None,):
    return {'text': text, 'color': color, 'on_color': on_color, 'attrs': attrs}

def cprint(text, color=None, on_color=None, attrs=None, **kwargs):
    if text:
        print((color_text(text, color, on_color, attrs)), **kwargs)

def cprintm(list_text_with_options, **kwargs):
    print(color_multiple_text(list_text_with_options))

if __name__ == '__main__':
    part1 = format_text_part('Hello ', 'yellow')
    part2 = format_text_part('World', 'red', attrs=['bold', 'underline'])
    cprintm([part1, part2])