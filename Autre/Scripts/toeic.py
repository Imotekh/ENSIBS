table = {
	0: {"list": 5, "read": 5},
	1: {"list": 5, "read": 5},
	2: {"list": 5, "read": 5},
	3: {"list": 5, "read": 5},
	4: {"list": 5, "read": 5},
	5: {"list": 5, "read": 5},
	6: {"list": 5, "read": 5},
	7: {"list": 5, "read": 5},
	8: {"list": 5, "read": 5},
	9: {"list": 5, "read": 5},
	10: {"list": 5, "read": 5},
	11: {"list": 5, "read": 5},
	12: {"list": 5, "read": 5},
	13: {"list": 5, "read": 5},
	14: {"list": 5, "read": 5},
	15: {"list": 5, "read": 5},
	16: {"list": 5, "read": 5},
	17: {"list": 5, "read": 5},
	18: {"list": 10, "read": 5},
	19: {"list": 15, "read": 5},
	20: {"list": 20, "read": 5},
	21: {"list": 25, "read": 5},
	22: {"list": 30, "read": 10},
	23: {"list": 35, "read": 15},
	24: {"list": 40, "read": 20},
	25: {"list": 45, "read": 25},
	26: {"list": 50, "read": 30},
	27: {"list": 55, "read": 35},
	28: {"list": 60, "read": 40},
	29: {"list": 70, "read": 45},
	30: {"list": 80, "read": 55},
	31: {"list": 85, "read": 60},
	32: {"list": 90, "read": 65},
	33: {"list": 95, "read": 70},
	34: {"list": 100, "read": 75},
	35: {"list": 105, "read": 80},
	36: {"list": 115, "read": 85},
	37: {"list": 125, "read": 90},
	38: {"list": 135, "read": 95},
	39: {"list": 140, "read": 105},
	40: {"list": 150, "read": 115},
	41: {"list": 160, "read": 120},
	42: {"list": 170, "read": 125},
	43: {"list": 175, "read": 130},
	44: {"list": 180, "read": 135},
	45: {"list": 190, "read": 140},
	46: {"list": 200, "read": 145},
	47: {"list": 205, "read": 155},
	48: {"list": 215, "read": 160},
	49: {"list": 220, "read": 170},
	50: {"list": 225, "read": 175},
	51: {"list": 230, "read": 185},
	52: {"list": 235, "read": 195},
	53: {"list": 245, "read": 205},
	54: {"list": 255, "read": 210},
	55: {"list": 260, "read": 215},
	56: {"list": 265, "read": 220},
	57: {"list": 275, "read": 230},
	58: {"list": 285, "read": 240},
	59: {"list": 290, "read": 245},
	60: {"list": 295, "read": 250},
	61: {"list": 300, "read": 255},
	62: {"list": 310, "read": 260},
	63: {"list": 320, "read": 270},
	64: {"list": 325, "read": 275},
	65: {"list": 330, "read": 280},
	66: {"list": 335, "read": 285},
	67: {"list": 340, "read": 290},
	68: {"list": 345, "read": 295},
	69: {"list": 350, "read": 295},
	70: {"list": 355, "read": 300},
	71: {"list": 360, "read": 310},
	72: {"list": 365, "read": 315},
	73: {"list": 370, "read": 320},
	74: {"list": 375, "read": 325},
	75: {"list": 385, "read": 330},
	76: {"list": 395, "read": 335},
	77: {"list": 400, "read": 340},
	78: {"list": 405, "read": 345},
	79: {"list": 415, "read": 355},
	80: {"list": 420, "read": 360},
	81: {"list": 425, "read": 370},
	82: {"list": 430, "read": 375},
	83: {"list": 435, "read": 385},
	84: {"list": 440, "read": 390},
	85: {"list": 445, "read": 395},
	86: {"list": 455, "read": 405},
	87: {"list": 460, "read": 415},
	88: {"list": 465, "read": 420},
	89: {"list": 475, "read": 425},
	90: {"list": 480, "read": 435},
	91: {"list": 485, "read": 440},
	92: {"list": 490, "read": 450},
	93: {"list": 495, "read": 455},
	94: {"list": 495, "read": 460},
	95: {"list": 495, "read": 470},
	96: {"list": 495, "read": 475},
	97: {"list": 495, "read": 485},
	98: {"list": 495, "read": 485},
	99: {"list": 495, "read": 490},
	100: {"list": 495, "read": 495}
}

grades = {
	"NULL/20": {"min": 0, "max": 119, "desc": "Qu'est ce que tu veux que je te dise ?"},
	"A1": {"min": 120,"max": 224, "desc": "Utilisateur élémentaire débutant"},
	"A2": {"min": 225,"max": 549, "desc": "Utilisateur élémentaire intermédiaire"},
	"B1": {"min": 550,"max": 784, "desc": "Utilisateur indépendant"},
	"B2": {"min": 785,"max": 944, "desc": "Utilisateur indépendant avancé"},
	"C1": {"min": 945,"max": 990, "desc": "Utilisateur expérimenté"}
}

if __name__ == '__main__':
	note_list = int(input("Your listening score : "))
	note_read = int(input("Your reading score : "))

	score_list = table.get(note_list).get("list")
	score_read = table.get(note_read).get("read")
	score_total = score_list + score_read
	grade_obtenu = "?"
	for grade in grades:
		if(score_total >= grades[grade]["min"] and score_total <= grades[grade]["max"]):
			grade_obtenu = grade
	
	print("Your score is : ", score_total)
	print("Your grade is : ", grade_obtenu)
	print("Grade descritpion : ", grades[grade_obtenu]["desc"])
	if(grade_obtenu == "B2"):
		print("Yeah you passed it for engineer grade !!")
	else:
		print("Oupsi doupsi you must retry :/")
