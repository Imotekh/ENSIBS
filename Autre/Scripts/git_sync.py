"""
Synchronize all submodules of a main repository following the configured FILE_STRUCT
And update submodules references in main repository
"""

from color_logger import cprintm, cprint, format_text_part as f_t_p
from shutil import copyfile
import subprocess
import os
import sys
import requests

REMOTE = 'origin'
BRANCH = 'master'

SM_SEP = '---------------------------------------------------------------------------------------'
LG_SEP = '======================================================================================='

FILE_STRUCT = {
    "1A": {
        "name": "Année 1",
        "sub": {
            "S1": {"name": "Semestre 1"},
            "S2": {"name": "Semestre 2"},
        },
    },
    "2A": {
        "name": "Année 2",
        "sub": {
            "S3": {"name": "Semestre 3"},
            "S4": {"name": "Semestre 4"},
        },
    },
    "3A": {
    	"name": "Année 3",
    	"sub": {},
    },
    "Global": {
        "name": "Global"
    }
}

def get_random_commit_message():
    """
    Get a wonderful commit message
    """

    r = requests.get("http://whatthecommit.com/index.txt")
    if r.status_code == 200:
        return r.text

def print_step(order, message):
    """
    Print a new step in the console

    Parameters:
        order (str): Place of the step in the process
        message (str): Message associated to the step
    """

    cprintm([
        f_t_p('[{}] '.format(order), 'yellow'),
        f_t_p(message, 'cyan')
    ])

def execute_command(command):
    """
    Execute a command in host shell
    Parameters:
        command (list): Words composing the command
    Returns:
        stdout (str): Standard command output
        stderr (str): Error command output
    """

    result = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, err = result.communicate();
    result.wait()
    sys.stdout.flush()
    return (stdout.decode('UTF-8'), err.decode('UTF-8'))

def commit_and_push_changes(commit_msg):
    """
    Commit changes and push them to remote host
    for the current repository program location

    Parameters:
        commit_msg (str): Le message de commit
    """

    print_step('3', "Enregistrement des changements :")

    execute_command(['git', 'add', '--all'])
    commit_res = execute_command(['git', 'commit', '-m', commit_msg])
    cprint(commit_res[0][:-1], 'grey')
    cprint(commit_res[1][:-1], 'grey')

    print_step('4', "Envoi des données sur le repos distant :")

    push_res = execute_command(['git', 'push', REMOTE, BRANCH])
    cprint(push_res[0][:-1], 'grey')
    cprint(push_res[1][:-1], 'grey')

    print_step('FIN', "Tous les changements ont été sauvegardés")

def check_for_head(order):
    """
    Réconcilie le HEAD et la branche voulue

    Parameters:
        order (str): Le numéro de cette étape
    """
    
    branch = execute_command(['git', 'branch'])[0]
    if not branch[2:-1] == BRANCH:
        print_step(order, "Réconciliation du HEAD et de {}".format(BRANCH))

        checkout_res = execute_command(['git', 'checkout', '-b', 'temp'])
        cprint(checkout_res[0][:-1], 'grey')
        cprint(checkout_res[1], 'grey')

        branch1_res = execute_command(['git', 'branch', '-f', BRANCH, 'temp'])
        cprint(branch1_res[0][:-1], 'grey')
        cprint(branch1_res[1][:-1], 'grey')

        branch2_res = execute_command(['git', 'branch', '-d', 'temp'])
        cprint(branch2_res[0][:-2], 'grey')
        cprint(branch2_res[1][:-2], 'grey')
    else:
        print_step(order, "Vous êtes déjà sur {}".format(BRANCH))

def get_subdirectory_list():
    """
    Get a list of subdirectories of the current working directory

    Returns:
        directory_list (list): A list of the subdirectories
    """

    directory_list = None
    for (dirpath, dirnames, filenames) in os.walk('.'):
        directory_list = dirnames
        break
    return directory_list

def recursive_dir_walk(dir_list, level, commit_msg):
    """
    Walk in the file structure to update all submodules and list them

    Parameters:
        dir_list (dict): File structure and repositories list
        level (int): Level of the current working directory in the main repository
        commit_msg (str): Le message de commit
    Returns:
        dir_list (dict): Updated file structure with found repositories
    """

    os.chdir(os.getcwd())
    for directory in dir_list:
        os.chdir(directory)

        if 'sub' in dir_list[directory]:
            result = recursive_dir_walk(dir_list[directory]['sub'], level+1, commit_msg)
            dir_list[directory]['sub'] = result

        dir_list[directory]['repos'] = update_submodules(level, commit_msg)

        os.chdir('..')
    return dir_list

def update_submodules(level, commit_msg):
    """
    Update all submodules contained in the current working directory

    Parameters:
        level (int): Level of the current working directory in the main repository
        commit_msg (str): Le message de commit
    Returns:
        repos (list): List of submodules found in the current working directory
    """

    repos = []
    for directory in get_subdirectory_list():
        if not directory.startswith('.'):
            os.chdir(directory)

            if os.path.isfile('./.git') or os.path.isdir('./.git'):

                remote_url = execute_command(['git', 'remote', 'get-url', REMOTE])[0][:-1]
                status_result = execute_command(['git', 'status'])[0]       

                print(SM_SEP)
                cprintm([
                    f_t_p('Répertoire : ', 'cyan'),
                    f_t_p(remote_url, 'yellow')
                ])
                print(SM_SEP)

                check_for_head('1')
                execute_command(['git', 'checkout', 'master'])  

                print_step('2', "Récuperation des données :")

                pull_res = execute_command(['git', 'pull', REMOTE, BRANCH, '--allow-unrelated-histories'])
                cprint(pull_res[0][:-1], 'grey')
                cprint(pull_res[1][:-1], 'grey')

                if "Untracked" in status_result:
                    commit_and_push_changes(commit_msg)
                else:
                    print_step('FIN', "Aucun changement à sauvegarder")

                repos.append({"name": directory, 'url': remote_url})
            os.chdir("..")
    return repos

def update_main_repository(commit_msg):
    """
    Update main repository and references to submodules

    Parameters:
        commit_msg (str): Le message de commit
    """

    check_for_head('0')
    execute_command(['git', 'checkout', 'master'])
    print_step('1', "Récuperation des données :")

    pull_res = execute_command(['git', 'pull', REMOTE, BRANCH, '--recurse-submodules'])
    cprint(pull_res[0][:-1], 'grey')
    cprint(pull_res[1][:-1], 'grey')

    print_step('2', "Mise à jour des références vers les sous-modules :")

    update_res = execute_command(['git', 'submodule', 'update', '--remote'])
    os.system('')
    cprint(update_res[0], 'grey')
    cprint(update_res[1], 'grey')

    commit_and_push_changes(commit_msg)

def sub_paraph(dir_list, file, level):
    """
    Add directory structure and repos list to README

    Parameters:
        dir_list (dict): File structure and repositories list
        file (File): README file to modify
        level (int): Level of indentation
    """

    for directory in dir_list:
        file.write('{}- {}:\n'.format('\t'*level, dir_list[directory]['name']))
        for matiere in dir_list[directory]['repos']:
            file.write('{}- [{} ]({})\n'.format('\t'*(level+1), matiere['name'], matiere["url"]))
        if 'sub' in dir_list[directory]:
            sub_paraph(dir_list[directory]['sub'], file, level+1)

def update_readme(repos):
    """
    Initialize README update

    Parameters:
        repos (dict): File structure and repositories list
    """

    with open('README.md', 'w') as file:

        file.write((
            '# ENSIBS\n'
            'Mon dépôt me permettant de stocker et synchroniser tous mes fichiers'
            'de cours à l\'ENSIBS dans la filière Cyberdéfense.\n'
            '\n## Les Cours :\n\n'
        ))

        sub_paraph(repos, file, 0)

def ask(question):
    """
    Ask a yes/no question to the user

    Parameters:
        question (str): The question to ask
    Returns:
        True/False (bool): La réponse de l'utilisateur
    """

    push = input(question)
    if push == "Y" or push == "y":
        return True
    else:
        return False

if __name__ == '__main__':
    os.system('')

    commit = ask("Do you want to specify a commit message ? (Y/N) : ")
    commit_msg = ""
    if commit:
        commit_msg = input("Commit message : ")
    else:
        commit_msg = get_random_commit_message()

    print(LG_SEP)
    cprint('            SYNCHRONISATION DES REPERTOIRES', 'red', attrs=['bold'])
    repos = recursive_dir_walk(FILE_STRUCT, 1, commit_msg)

    print(LG_SEP)
    cprint('                UPDATE README', 'red', attrs=['bold'])
    update_readme(repos)
    
    print(LG_SEP)
    cprint('            SYNCHRONISATION DES SOUS-MODULES', 'red', attrs=['bold'])
    print(LG_SEP)
    update_main_repository(commit_msg)
    