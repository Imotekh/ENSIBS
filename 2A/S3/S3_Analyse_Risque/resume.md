Lectures

[Résumé DSP2](http://www.fbf.fr/fr/files/AU8CQN/MEMO_08_DSP2.pdf)

[Article CNIL](https://www.cnil.fr/fr/le-paiement-distance-par-carte-bancaire)

# En quoi consiste la DSP2

DSP2 : Petit nom amicale de la norme UE 2015/2366 relative aux service de paiement dans le marché intérieur. Son but est de *sécuriser* et de *faciliter* l'utilisation des services de *paiement numériques* en normalisant les échanges entre banques et fournisseurs de service de paiement. Applicable à partir de janvier 2018.

2 obligations principales : 

1. Les banques doivent fournir une API standardisée permettant à un tiers d'initier un paiement depuis le compte d'un de leur client 
2. Les banques doivent rembourser le client en cas de fraude sur l'initiation de paiement

Gros volume de données à traiter : 

> 54,8 Milliards de paiements différents par CB en 2018 selon la BC

Standard d'authentification forte établis par l'autorité bancaire européenne (ABE) applicable à partir de septembre 2019. Le but est de faire une transition vers le 2FA obligatoire. Au début par les banques qui sont obligées de le faire de manière ponctuelle (90 jour). Puis par les sites permettant le paiement en ligne qui sont déjà invités à le faire.

Notion de protection des données : l'opération de vérification du paiement doit se faire sans exposer plus de données que nécessaire. La CNIL rappelle que les seules données obligatoires pour le paiement sont : 

1. Le numéro de carte
2. La date d'expiration
3. Le cryptogramme visuel

Et que ces données n'ont pas besoin d'être conservée à l'issu de la transaction. Si ces données ont besoin d'être conservé cela doit se faire avec l'accord volontaire non équivoque [1]() du client et à une des finalités suivants : 

- Paiement d'un bien ou d'un service
- Règlement multiple d'un abonnement souscrit en ligne
- Réservation d'un bien ou d'un service
- De ce que j'ai compris, un dernier point obscur concernant le B2B des prestataires de service de paiement (qu'est ce que ça fout sur un résumé ??)

La conservation des données bancaires pour faciliter de futures paiement peut se faire seulement si le client exprime explicitement le souhait de souscrire a un service facilitant les achats. Si le client accède à un compte premium cette souscription peut se faire implicitement à condition :

- d'être clairement mentionnée (visible + explicite)
- de permettre une opposition sans conséquence sur l'accès au service
- d'être sécurisé :DDDD

Les autres données qui ne sont pas nécessaires à la reconduction du paiement ne doivent pas être stockée. 

Cf doc pour les durées de rétention et les préco de sécurité émises par la cnil.



# Fevad livre blanc

### TL; DR 

ça me semble inutilement alarmiste comme tableau, les pentesteurs en herbe vous me confirmez que ça correspond à la réalité du terrain ? (dev pas formés aux sql injection oO)

## Principales causes de vulnérabilité

Contrairement aux infra les applicatifs ne sont généralement pas encore conçus pour la sécurité. Beaucoup de sites de e-commerce qui font l'objet de dév spécifique par des dévs mal informés.

>  Arnaud Treps constate qu’il n’existe pas ou peu d’école du hacking. Il n’y a pas d’autre choix que celui de l’autoformation.

-> pétition pour envoyer à M. Arnaud Treps une carte postale signée bon baiser de l'ensibs.

Les étudiants sont nulles : no shit :)))) 

Nécessité de faire de la maintenance et de la mise à jour particulièrement pour les projets open-source dont les màj peut être fréquente. Quand on intègre pas les mise à jour automatiquement la charge de travail est lourde. Si on les intègre alors c'est le risque de tout casser qui survient.

Not getting paid enough for this shit -> les gens n'ont pas le time de s'occuper de cyber.

## Questions restées ouvertes

> Cette interface se substitue aux techniques de « web scraping », basées sur l’utilisation par les agrégateurs et initiateurs de paiement des identifiants et des mots de passe des clients, transmis par ces derniers sous leur seule responsabilité.



WTF pour tracer les comptes il fallait avant faire du scrapping avec les logins mdps utilisateurs oO

