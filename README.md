# ENSIBS
Mon d�p�t me permettant de stocker et synchroniser tous mes fichiersde cours � l'ENSIBS dans la fili�re Cyberd�fense.

## Les Cours :

- Ann�e 1:
	- [1A_Projet_IS ](https://gitlab.com/Imotekh/ENSIBS-Projet_IS.git)
	- [1A_Veille ](https://gitlab.com/Imotekh/ENSIBS-Veille.git)
	- Semestre 1:
		- [S1_Algo_Prog ](https://gitlab.com/Imotekh/ENSIBS-S1_Algo_Prog.git)
		- [S1_BDD ](https://gitlab.com/Imotekh/ENSIBS-S1_BDD.git)
		- [S1_Economie ](https://gitlab.com/Imotekh/ENSIBS-S1_Economie.git)
		- [S1_Electronique ](https://gitlab.com/Imotekh/ENSIBS-S1_Electronique.git)
		- [S1_Ingenierie_Systeme ](https://gitlab.com/Imotekh/ENSIBS-S1_Ingenierie_Systeme.git)
		- [S1_Marketing ](https://gitlab.com/Imotekh/ENSIBS-S1_Marketing.git)
		- [S1_Python ](https://gitlab.com/Imotekh/ENSIBS-S1_Python.git)
		- [S1_Signaux ](https://gitlab.com/Imotekh/ENSIBS-S1_Signaux.git)
	- Semestre 2:
		- [S2_Architecture_Ordinateur ](https://gitlab.com/Imotekh/ENSIBS-S2_Architecture_Ordinateur.git)
		- [S2_Ethique ](https://gitlab.com/Imotekh/ENSIBS-S2_Ethique.git)
		- [S2_Gestion_Financiere ](https://gitlab.com/Imotekh/ENSIBS-S2_Gestion_Financiere.git)
		- [S2_Mathematiques ](https://gitlab.com/Imotekh/ENSIBS-S2_Mathematiques.git)
		- [S2_Portfolio ](https://gitlab.com/Imotekh/ENSIBS-S2_Portfolio.git)
		- [S2_Prog_C ](https://gitlab.com/Imotekh/ENSIBS-S2_Prog_C.git)
		- [S2_Prog_Web ](https://gitlab.com/Imotekh/ENSIBS-S2_Prog_Web.git)
		- [S2_Systeme_Exploitation ](https://gitlab.com/Imotekh/ENSIBS-S2_Systeme_Exploitation.git)
		- [S2_Virtualisation ](https://gitlab.com/Imotekh/ENSIBS-S2_Virtualisation.git)
- Ann�e 2:
	- [2A_YOVO ](https://gitlab.com/open-yovo/meta.git)
	- Semestre 3:
		- [S3_Anglais ](https://gitlab.com/Imotekh/ENSIBS-S3_Anglais.git)
		- [S3_Base_Reseaux ](https://gitlab.com/Imotekh/ENSIBS-S3_Base_reseaux.git)
		- [S3_Securite_Android ](https://gitlab.com/Imotekh/ENSIBS-S3_Securite_Android.git)
		- [S3_Securite_Linux ](https://gitlab.com/Imotekh/ensibs-s3_securite_linux.git)
		- [S3_Securite_Reseaux ](https://gitlab.com/Imotekh/ensibs-s3_securite_reseaux.git)
		- [S3_Securite_Windows ](https://gitlab.com/Imotekh/ensibs-s3_securite_windows.git)
	- Semestre 4:
		- [S4_Analyse_Malware ](https://gitlab.com/Imotekh/ensibs-s4_analyse_malware.git)
		- [S4_Architecture_Securite ](https://gitlab.com/Imotekh/ensibs-s4_architecture-securite.git)
		- [S4_Cryptologie_Materielle ](https://gitlab.com/Imotekh/ensibs-s4_cryptologie_materielle.git)
		- [S4_Identite_Numerique ](https://gitlab.com/Imotekh/ensibs-s4_identite_numerique.git)
		- [S4_Programmation_Defensive ](https://gitlab.com/Imotekh/ensibs-s4_programmation_defensive.git)
		- [S4_Securite_des_Annuaires ](https://gitlab.com/Imotekh/ensibs-s4_securite-des-annuaires.git)
		- [S4_Securite_des_BDD ](https://gitlab.com/Imotekh/ensibs-s4_securite_des_bdd.git)
		- [S4_Securite_Messagerie ](https://gitlab.com/Imotekh/ensibs-s4_securite_messagerie.git)
		- [S4_Securite_Web ](https://gitlab.com/Imotekh/ensibs-s4_securite_web.git)
		- [S4_Steganographie ](https://gitlab.com/Imotekh/ensibs-s4_steganographie.git)
- Ann�e 3:
	- [3A_PFE ](https://gitlab.com/Imotekh/ensibs-3a_pfe.git)
	- [3A_Projet ](https://gitlab.com/bob_project/meta.git)
	- [S5_Anticipation ](https://gitlab.com/Imotekh/ensibs-s5_anticipation.git)
	- [S5_Audit_Cyber ](https://gitlab.com/Imotekh/ensibs-s5_audit_cyber.git)
	- [S5_Cyber_Threat_Intel ](https://gitlab.com/Imotekh/ensibs-s5_cyber_threat_intel.git)
	- [S5_Decision_Crise ](https://gitlab.com/Imotekh/ensibs-s5_decision_crise.git)
	- [S5_Forensic ](https://gitlab.com/Imotekh/ensibs-s5_forensic.git)
	- [S5_Forensic_Mobile ](https://gitlab.com/Imotekh/ensibs-s5_forensic_mobile.git)
	- [S5_Management ](https://gitlab.com/Imotekh/ensibs-s5_management.git)
	- [S5_Menace_Systeme_IT ](https://gitlab.com/Imotekh/ensibs-s5_menace-systeme-it.git)
	- [S5_OSINT ](https://gitlab.com/Imotekh/ensibs-s5_osint.git)
	- [S5_PPP ](https://gitlab.com/Imotekh/ensibs-s5_ppp.git)
	- [S5_Pratique_Administration ](https://gitlab.com/Imotekh/ensibs-s5_pratique-administration.git)
	- [S5_Relation_Client ](https://gitlab.com/Imotekh/ensibs-s5_relation-client.git)
	- [S5_Resilience_Remediation ](https://gitlab.com/Imotekh/ensibs-s5_resilience-remediation.git)
	- [S5_Strategie_Entreprise ](https://gitlab.com/Imotekh/ensibs-s5_strategie-entreprise.git)
	- [S5_Test_Intrusion ](https://gitlab.com/Imotekh/ensibs-s5_test_intrusion.git)
- Global:
	- [Alternance ](https://gitlab.com/Imotekh/ENSIBS-Alternance.git)
	- [Cours ](https://gitlab.com/Imotekh/ENSIBS-Cours.git)
